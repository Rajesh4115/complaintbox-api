var express = require("express");
var router = express.Router();
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

var notify = require("../mailer/notify");
var User = require("../models/user");
var Complaint = require("../models/complaint");

router.post('/login', function(req, res, next) {
	console.log(req.body);
	
	// User.findOne({email: req.body.email}, function(err, user){
	// 	if(err) {
	// 		return res.status(500).json({
	// 			title: "An error occurred",
	// 			error: err
	// 		});
	// 	}
	// 	if(!user) {
	// 		return res.status(401).json({
	// 			title: "Login failed",
	// 			error: { message: "Invalid login credentials" }
	// 		});
	// 	}

	// 	if(!bcrypt.compareSync(req.body.password, user.password)){
	// 		return res.status(401).json({
	// 			title: "Login failed",
	// 			error: { message: "Invalid login credentials" }
	// 		});
	// 	}
	// 	console.log("Successful");
	// 	var token = jwt.sign({user: user}, 'secret', {expiresIn: 7200});
	// 	res.status(200).json({
	// 		message: "Successfully logged in",
	// 		token: token,
	// 		userId: user._id
	// 	})
	// });
	if(req.body.username == "admin" && req.body.password == "admin") {
		var token = jwt.sign({user: req.body.username}, 'secret', {expiresIn: 7200});
		res.status(200).json({
			message: "Successfully logged in",
			token: token,
			type: 'admin'
		})
	} else {
		return res.status(401).json({
			title: "Login failed",
			error: { message: "Invalid login credentials" }
		});
	}
    
});

//Get complaints
router.get("/complaints", function(req, res, next) {
	Complaint.find({"resolved": false})
	  .populate("user", "name")
	  .populate("voters", "name")
	  .exec(function(err, complaints) {
		if (err) {
		  return res.status(500).json({
			title: "An error occurred",
			error: err
		  });
		}
		res.status(200).json({
		  message: "Success",
		  obj: complaints
		});
	  });
});

//Get public complaints
router.get("/complaints/public", function(req, res, next) {
	Complaint.find({"resolved": false, type:"public"})
	  .populate("user", "name")
	  .populate("voters", "name")
	  .exec(function(err, complaints) {
		if (err) {
		  return res.status(500).json({
			title: "An error occurred",
			error: err
		  });
		}
		res.status(200).json({
		  message: "Success",
		  obj: complaints
		});
	  });
});

//Get public complaints
router.get("/complaints/private", function(req, res, next) {
	Complaint.find({"resolved": false, type:"private"})
	  .populate("user", "name")
	  .populate("voters", "name")
	  .exec(function(err, complaints) {
		if (err) {
		  return res.status(500).json({
			title: "An error occurred",
			error: err
		  });
		}
		res.status(200).json({
		  message: "Success",
		  obj: complaints
		});
	  });
});

// Resolve
router.post("/complaints/resolve/:id", function(req, res) {
  Complaint.findById(req.params.id, function(err, foundComplaint) {
    if (err) {
      return res.status(500).json({
        title: "Complaint not found",
        error: err
      });
    } else {
      
      foundComplaint.resolved = true;
      Complaint.findByIdAndUpdate(foundComplaint._id, foundComplaint, function(
        err,
        updatedComplaint
      ) {
        if (err) {
          return res.status(500).json({
            title: "An error occurred",
            error: err
          });
        }

        res.status(201).json({
          message: "Marked as resolved!",
          obj: updatedComplaint
        });
      });
    }
  });
});

module.exports = router;