var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

var User = require('../models/user');
var Complaint = require('../models/complaint');


router.get('/', function(req, res, next) {
	var auth = req.headers.authorization; 
  	jwt.verify(auth, 'secret', function(err, decoded){
		if(err) {
			return res.status(401).json({
				verified: false
			});
		}
		let obj = {
			user: {},
			complaints: {}
		}
		User.findOne({_id: decoded.user._id}, function(err, user){
			Complaint.find({'user': user._id}, function(err, complaints){
				if(err)
					console.log(err);
				console.log(complaints);
				
				obj.user = user;
				obj.complaints = complaints;
				console.log(obj);
				
				res.status(200).json(obj);
			})
			
		})
		
	});
});

router.post('/register', function(req, res, next){
	var user = new User({
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password1, 10),
		name: req.body.name,
		mobile: req.body.mobile,
		admission_no: req.body.admission_no,
		department: req.body.department,
		semester: req.body.semester,

	});
	user.save(function(err, result){
		if(err) {
			return res.status(500).json({
				title: "An error occurred",
				error: err
			});
		}
		res.status(201).json({
			message: "User created",
			obj: result
		});
	});
});

router.post('/signin', function(req, res, next) {
	console.log(req.body);
	
	User.findOne({email: req.body.email}, function(err, user){
		if(err) {
			return res.status(500).json({
				title: "An error occurred",
				error: err
			});
		}
		if(!user) {
			return res.status(401).json({
				title: "Login failed",
				error: { message: "Invalid login credentials" }
			});
		}

		if(!bcrypt.compareSync(req.body.password, user.password)){
			return res.status(401).json({
				title: "Login failed",
				error: { message: "Invalid login credentials" }
			});
		}
		console.log("Successful");
		var token = jwt.sign({user: user}, 'secret', {expiresIn: 7200});
		res.status(200).json({
			message: "Successfully logged in",
			token: token,
			userId: user._id,
			name: user.name,
			type: "student"
		})
	});
});

router.patch('/:id/:action', function(req, res, next){

	console.log(req.params.id);
	User.findById(req.params.id, function (err, user){
			if(err) {
				return res.status(500).json({
					title: 'An error occurred',
					error: err

				});
			}
			if(!user) {
				return res.status(500).json({
					title: 'No user found',
					error: {message: 'User not found'}
				});
			}

			// for updating user
			if(req.params.action === 'update'){
				user.name = req.body.name;
				user.email = req.body.email;
				user.department = req.body.department;
				user.admission_no = req.body.admission_no;
				user.mobile = req.body.mobile;
				user.semester = req.body.semester;

				user.save(function(err, result){
					if(err) {
						return res.status(500).json({
							title: 'An error occurred',
							error: err
						});
					}
					res.status(200).json({
						message: 'Updated user',
						user: result
					});
				});
			} 

			//  For chenging password
			if(req.params.action === 'changePassword') {
				console.log(req.body);
				if(req.body.newPassword !== req.body.confirmNewPassword) {
					return res.status(500).json({
						title: 'An error occurred',
						error: {message: "New password and Confirm password didn't match"}
					});
				}
				if(!bcrypt.compareSync(req.body.oldPassword, user.password)){
					return res.status(401).json({
						title: "An error occurred",
						error: { message: "Old passsword is not correct" }
					});
				}
				console.log('validated');
				user.password =  bcrypt.hashSync(req.body.newPassword, 10),
				user.save(function(err, result){
					if(err) {
						return res.status(500).json({
							title: 'An error occurred',
							error: err
						});
					}
					res.status(200).json({
						message: 'Updated user',
						user: result
					});
				});
			}
		});
});

router.post('/verify', function(req, res, next){
	jwt.verify(req.query.token, 'secret', function(err, decoded){
		if(err) {
			return res.status(401).json({
				verified: false
			});
		}
		console.log(decoded.user);
		res.status(200).json({
			verified: true
		});
	});
});

module.exports = router;
