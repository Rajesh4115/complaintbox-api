var express = require("express");
var router = express.Router();
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

var notify = require("../mailer/notify");
var User = require("../models/user");
var Complaint = require("../models/complaint");

// GET ALL COMPLAINTS
router.get("/", function(req, res, next) {
  Complaint.find({ type: "public", resolved: false })
    .populate("user", "name")
    .populate("voters", "name")
    .exec(function(err, complaints) {
      if (err) {
        return res.status(500).json({
          title: "An error occurred",
          error: err
        });
      }
      res.status(200).json({
        message: "Success",
        obj: complaints
      });
    });
});

// GET TOP 10 VOTED COMPLAINTS
router.get("/top10", function(req, res, next) {
  Complaint.find({ type: "public", resolved: false })
    .populate("user", "name")
    .populate("voters", "name")
    .sort({'upvotes': -1})
    .limit(10)
    .exec(function(err, complaints) {
      if (err) {
        return res.status(500).json({
          title: "An error occurred",
          error: err
        });
      }
      res.status(200).json({
        message: "Success",
        obj: complaints
      });
    });
});

// GET A SINGLE COMPLAINT
router.get("/:id", function(req, res, next) {
  Complaint.findById(req.params.id).exec(function(err, complaint) {
    if (err) {
      return res.status(500).json({
        title: "An error occurred",
        error: err
      });
    }
    res.status(200).json({
      message: "Success",
      obj: complaint
    });
  });
});


// check authentication by verifying token
router.use("/", function(req, res, next) {
  var auth = req.headers.authorization;
  jwt.verify(auth, "secret", function(err, decoded) {
    if (err) {
      return res.status(401).json({
        title: "Not Authenticated",
        error: err
      });
    }
    next();
  });
});

// POST A NEW COMPLAINT
router.post("/", function(req, res, next) {
  var decoded = jwt.decode(req.headers.authorization);
  User.findById(decoded.user._id, function(err, user) {
    if (err) {
      return res.status(500).json({
        title: "An error occurred",
        error: err
      });
    }
    var complaint = new Complaint({
      type: req.body.type,
      category: req.body.category,
      title: req.body.title,
      description: req.body.description,
      created: new Date(),
      user: user
    });
    complaint.save(function(err, result) {
      if (err) {
        return res.status(500).json({
          title: "An error occurred",
          error: err
        });
	  }
	  
      if (result.type == "private") {
        notify.sendNotification(result);
	  }
	  
      res.status(201).json({
        message: "Complaint posted",
        obj: result
      });

      // TODO: Send mail to the user
    });
  });
});

// + vote
router.patch("/:id/votes", function(req, res) {
  Complaint.findById(req.params.id, function(err, foundComplaint) {
    if (err) {
      return res.status(500).json({
        title: "Complaint not found",
        error: err
      });
    } else {
      var decoded = jwt.decode(req.headers.authorization);
      if (foundComplaint.voters.indexOf(decoded.user._id) >= 0) {
        return res.status(500).json({
          title: "An error occurred",
          error: { message: "You have already voted for that complaint" }
        });
      }
      ++foundComplaint.votes;
      foundComplaint.voters.push(decoded.user._id);
      Complaint.findByIdAndUpdate(foundComplaint._id, foundComplaint, function(
        err,
        updatedComplaint
      ) {
        if (err) {
          return res.status(500).json({
            title: "An error occurred",
            error: err
          });
        }

        res.status(201).json({
          message: "Upvoted Succesfully",
          obj: updatedComplaint
        });
      });
    }
  });
});

module.exports = router;
