var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var mongooseUniqueValidator = require('mongoose-unique-validator');

var schema = new Schema ({
	password: {type: String, required: true},
	email: { type: String, required: true, unique: true},
	name: String,
    mobile: String,
	admission_no: String,
	department: String,
	semester: String,
	permalink: String,
	month_start: Date,  // store the date when a month starts
	no_private_complaints: {
		type: Number,
		default: 0
	}, 	// store no. of private complaints in month
	verified: {
		type: Boolean,
		default: false
	},
	verify_token: String
});

schema.plugin(mongooseUniqueValidator);

module.exports = mongoose.model('User', schema);