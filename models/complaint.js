var mongoose = require("mongoose");
var complaintSchema = new mongoose.Schema({
	type: String,
	category: String,
	title: String,
	description: String,
	created: Date,
	resolved: {
		type: Boolean,
		default: false
	},
	votes: {
		type: Number,
		default: 0
	},
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "User"	
	},
	voters: [
		{
			type: mongoose.Schema.Types.ObjectId,
			ref: "User"
		}
	]
});

module.exports = mongoose.model("Complaint", complaintSchema);