const nodemailer = require("nodemailer");
const credentials = require('../config/gmail');
var mailObj = {};

mailObj.sendMail = function(mailID, sub, content) {
  let transporter = nodemailer.createTransport({
    // service: 'gmail',
    // auth: {
    //     user: 'raj4115vbn@gmail.com',
    //     pass: 'raj@5114'
    // }
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth: {
      type: "OAuth2",
      user: credentials.user,
      clientId: credentials.clientId,
      clientSecret: credentials.clientSecret,
      refreshToken: credentials.refreshToken,
      accessToken: credentials.accessToken,
      expires: 1484314697598
    }
  });

  const mailOptions = {
    from: "raj4115vbn@gmail.com",

    to: mailID,

    subject: sub, //

    html: content
  };
  console.log("Sending Mail");
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      //    req.flash(error.message);
      console.log(error);
      return;
    }
    console.log("Message sent successfully!", info.messageId);
    // req.flash("success", "Mail Sent");
    console.log('Server responded with "%s"', info.response);
    transporter.close();
  });
};

module.exports = mailObj;
