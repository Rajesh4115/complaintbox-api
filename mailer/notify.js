const mail = require("./mail.js");

var notifyObj = {};

// send mail to concerning authority
notifyObj.sendNotification = function(newComplaint){

    let address = "rajesh.vbn@gmail.com",
        subject = "New Complaint: ",
        content = "";
    console.log("Complaint info: " + newComplaint);
    
    content = `<p>A new Complaint has been registered! <a href='http://localhost:3000/complaints/${newComplaint._id}'>Click Here to Open</a></p>`
        switch ( newComplaint.category) {

            case "faculty" :
                            subject += "Faculty";
                            break;
            case "hostel" :
                            subject += "Hostel";
                            break;

            case "transport" :
                            subject += "Transport";
                            break;

            case "campus" :
                            subject += "Campus";
                            break;
            case "class" :
                            subject += "Class";
                            break;
            case "accounts" :
                            subject += "Accounts";
                            break;
            case "students-section" :
                            subject += "Students Section";
                            break;
            case "maintenance" :
                            subject += "Maintenance";
                            break;
            case "other" :
                            subject += "Other";
                            break;
            default :
                        break;
        }
        mail.sendMail(address, subject, content);
}

// Send mail to the user
notifyObj.sendConfirmation = function(newComplaint){
    console.log("Complaint info: " + newComplaint);
    if ( newComplaint.type == "private" ) {
        mail.sendMail("rajesh.vbn@gmail.com", "Public", newComplaint.description);
    }
}

module.exports = notifyObj;
